<!-- omit in toc -->
# Testing Airflow

- [Unit Tests](#unit-tests)
- [Integration tests](#integration-tests)
  - [Airflow Cli](#airflow-cli)
  - [dag.test()](#dagtest)
- [References](#references)

## Unit Tests

We can do regular unit testing and mocking with `pytest`.  Everything is unit testable:

- custom airflow objects
  - hooks
  - operators
  - sensors
  - these are all straight forward to test and there are lots of examples in the airflow source code
- the python functions used in PythonOperators
- dags
  - these are less straight forward to test and there isn't a standard way to test them
  - The unit tests **should not be running the dag**, but verifiying its configuration.  Running a dag requires an airflow instance, and then the test would be an integration test, not a unit test.  To unit test the functionatlity of the dag, test the tasks individually.
  - task groups are probably testable in a similar way, but there are little to no examples of developers doing this

## Integration tests

### Airflow Cli

The airflow cli offers testing commands for dags and tasks.  These will run the code in a local instance of Airflow (but without saving any metadata in the DB).  Since it won't mock out the external resources, this would only be useful for integration testing with real resources in azure.  It also looks like it is a simple pass/fail and we can't design test assertions.  This may not be something we want to focus on over unit tests in our first iteration.

Commands:

```bash
airflow tasks test [dag_id] [task_id] [(optional) date]
airflow dags test [dag_id] [(optional) date]
```

**How do we set config values in these commands so that we can specify test resources?**  

- Looks like we can overwrite env variables with `--env-vars [env_vars]` (passed in as json)
- For task tests, we can pass `--task-params [task_params]` (passed in as json)
  - If we want to test our templating, and not run code, we can use `--dry-run` **but this still requires a local airflow instance**
  - For tasks only? didn't see that option at the dag level
- For dag tests, we can pass `--conf [conf]` (passed in as json)
  - neat options for rendering the dag run afterwards: `--imgcat-dagrun`, `--save-dagrun [file]`, `--show-dagrun`

**Will this be running locally (with the Kubernetes Executor)?  Does this run against an existing instance of airflow?**

- This requires initialization of a locally running airflow (uses docker and some sort of local sql with sqlalchemy)
- We could set the executor type in the local airflow config I think, along with other config values for the tests.
- We could also use a local astro to simplify the local airflow instance

### dag.test()

This looks more usable than the cli commands.

You can test dags in a single python process (no executor) with debugging enabled by adding the following to your dag file:

```python
@dag(...)
def my_dag():
    t1 = DummyOperator(task_id="t1")

dag_obj = my_dag()

if __name__ == "__main__":
    dag_obj.test()
```

You can pass dag arguments to `test()` to set config values for your testing env.

```python
if __name__ == "__main__":
    conn_path = "connections.yaml"
    variables_path = "variables.yaml"
    my_conf_var = 23

    dag.test(
        execution_date=datetime(2023, 1, 29),
        conn_file_path=conn_path,
        variable_file_path=variables_path,
        run_conf={"my_conf_var": my_conf_var},
    )
```

Now when you run your dag file with python, it will call dag.test.

**Is this still an integration test that requires local airflow and external resources?**

- yes, this is basically the same as the cli commands, but you can use a debugger with it and it's easier to configure
- also, becuase there is no executor, we can't test things that require the Kubernetes Executor pod modifications.

## References

- [Airflow Fundamentals Testing](https://airflow.apache.org/docs/apache-airflow/stable/tutorial/fundamentals.html#id2)
- [Airflow dag.test()](https://airflow.apache.org/docs/apache-airflow/stable/core-concepts/executor/debug.html)
- [Astro Documentation](https://docs.astronomer.io/learn/testing-airflow?tab=decorator#debug-interactively-with-dagtest)
- [Example of Unit Testing DAGs](https://medium.com/@jw_ng/writing-unit-tests-for-an-airflow-dag-78f738fe6bfc)
- [Airflow Source Code Tests](https://github.com/apache/airflow/tree/main/tests)
