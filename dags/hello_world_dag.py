from datetime import datetime

from airflow.decorators import dag, task

@task.python()
def say_hello_py():
    print("Hello World!")

@dag(description="DAG for saying \"Hello World!\"",
    start_date=datetime(2023, 1, 1),
    catchup=False)
def hello_world_dag():
    say_hello_py()

hello_world_dag()
