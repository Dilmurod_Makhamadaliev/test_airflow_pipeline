# Devcontainer setup

## What is a Devcontainer and why its benificial

Development containers are a VS Code feature that allows developers to package a local development tool stack into the internals of a Docker container while also bringing the VS Code UI experience with them. It is a pre-configured, isolated environment for developing software that can be run on a remote machine or locally. It includes all the necessary tools, libraries, and dependencies required to develop, test, and debug code in a consistent and reproducible manner.They're configured using an image, Dockerfile, or Docker Compose file and devcontainer.json, which is a metadata format used to enrich containers with development specific content and settings

There are several benefits to using devcontainers:

1. Consistent Development Environment: Devcontainers ensure that every developer on a project is working with the same tools and configurations that is reproducible across multiple machines. This helps to prevent issues caused by different versions of tools or configurations.

2. Isolation: Devcontainers run within Docker containers, which provide a degree of isolation from the host machine and other containers. This helps to prevent conflicts between different projects or development environments

3. Portability: Devcontainers can be run on a remote machine or locally, making it easy to develop code in a consistent environment across different machines.

4. Efficiency: Devcontainers can be preconfigured with the necessary dependencies, tools, and extensions for a specific project or technology stack. This reduces the time and effort required to set up a development environment, and allows developers to focus on writing code rather than configuring their machines.

## Prerequisites

- [Docker](https://docs.docker.com/get-docker/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [Install Devcontainer extension](vscode:extension/ms-vscode-remote.remote-containers)

## Setup

1. Ensure the docker is running locally
2. Open VS Code and ensure the devcontainer extension is installed. If not, install it using the above devcontiner extension link or search for `Dev Containers` on the vs code side pannel for installing exntensions.
3. Access to vs code command palette using (Ctrl+Shift+P) on Windows or (CMD+SHIFT+P) on MAC and select `Dev-Containers: Open Folder in Container...`
4. Chose a `.devcontainer` folder path.

## Resources:

[CSE Engineering Playbook](https://microsoft.github.io/code-with-engineering-playbook/developer-experience/devcontainers/)
[Dev Containers tutorial](https://code.visualstudio.com/docs/devcontainers/tutorial)
[More about Devcontainers](https://code.visualstudio.com/blogs/2022/09/15/dev-container-features)
[Isolate your Visual Studio Code workspace](https://medium.com/@toonsev/dev-containers-isolate-your-visual-studio-code-workspaces-a2fed6c60606)

[Dev Containers tutorial](https://code.visualstudio.com/docs/devcontainers/tutorial)