from dags.hello_world_dag import say_hello_py, hello_world_dag

class TestHelloWorld:

    def test_say_hello(self, capfd):
        expected_output = "Hello World!\n"
        say_hello_py.function()
        captured = capfd.readouterr()
        actual_output = captured.out
        assert expected_output == actual_output

    def test_dag_only_says_hello(self):
        dag = hello_world_dag()
        assert dag.task_count == 1
        assert dag.task_ids[0] == "say_hello_py"
